package com.google.android.ads.admob.listener;

public abstract class AdMobViewLoadCallback {

    public void onAdLoaded() {
    }

    public void onAdOpened() {
    }

    public void onAdClicked() {
    }

    public void onAdClosed() {
    }

    public void onAdImpression() {
    }

    public void onAdFailedToLoad(String error) {
    }
}
