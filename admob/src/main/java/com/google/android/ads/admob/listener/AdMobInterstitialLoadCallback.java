package com.google.android.ads.admob.listener;

public abstract class AdMobInterstitialLoadCallback {

    public void onAdLoaded() {}

    public void onAdFailedToLoad(String error) {}

    public void onAdShowedFullScreenContent() {}

    public void onAdDismissedFullScreenContent() {}

    public void onAdFailedToShowFullScreenContent(String error) {}

    public void onAdImpression() {}

}
