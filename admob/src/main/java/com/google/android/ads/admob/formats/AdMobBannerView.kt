package com.google.android.ads.admob.formats

import android.content.Context
import android.os.Build
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.google.android.ads.R
import com.google.android.ads.admob.listener.AdMobViewLoadCallback
import com.google.android.gms.ads.*
import com.wang.avi.AVLoadingIndicatorView
import java.util.*

/**
 * Author: Med Ajaroud
 * Created At: 06/02/2020
 * Github: https://github.com/mrajaroud
 * License: Non-commercial
 */

/** Base class for a native ad template view. *  */
class AdMobBannerView : LinearLayout {
    var adMobViewLoadCallback: AdMobViewLoadCallback? = null

    private var isLoadAnimationBeforeLoadAd: Boolean = false
    private var animationAccentColor = 0

    private var adView: AdView? = null
    private var bannerContainer: LinearLayout? = null
    private var inflater: LayoutInflater? = null

    private var avLoadingIndicatorView: AVLoadingIndicatorView? = null
    var adLoaderBuilder: AdLoader.Builder? = null

    private var adViewUnitId: String? = null
    private var adViewSize: AdSize? = null


    constructor(context: Context?) : super(context) {
        this.initView(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        this.initView(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        this.initView(context, attrs)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        this.initView(context, attrs)
    }

    private fun initView(context: Context?, attributeSet: AttributeSet?) {
        this.inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val attributes = context.theme.obtainStyledAttributes(attributeSet, R.styleable.AdMobBannerView, 0, 0)
        try {
            this.isLoadAnimationBeforeLoadAd = attributes.getBoolean(R.styleable.AdMobBannerView_banner_animation, true)
            this.animationAccentColor = attributes.getColor(R.styleable.AdMobBannerView_banner_animation_accent_color, ContextCompat.getColor(context, android.R.color.holo_red_light))

            this.adViewUnitId = attributes.getString(R.styleable.AdMobBannerView_banner_unit_id)
            initSize(attributes.getInt(R.styleable.AdMobBannerView_banner_unit_size, -1))
        } finally {
            attributes.recycle()
        }
        initViews()
    }

    private fun initViews() {
        inflate(context, R.layout.admob_banner_layout, this)

        this.bannerContainer = findViewById(R.id.banner_container)
        this.avLoadingIndicatorView = findViewById(R.id.avl_indicator_view)
        this.avLoadingIndicatorView?.setIndicatorColor(animationAccentColor)
        this.avLoadingIndicatorView?.setIndicator(loadingAnimationType[Random().nextInt(loadingAnimationType.size)])
        viewsVisibility()
    }

    override fun getOrientation(): Int {
        return HORIZONTAL
    }

    private fun viewsVisibility() {
        if (isLoadAnimationBeforeLoadAd) {
            avLoadingIndicatorView?.visibility = View.VISIBLE
            bannerContainer?.visibility = View.GONE
            return
        }
        avLoadingIndicatorView?.visibility = View.GONE
        bannerContainer?.visibility = View.VISIBLE
    }

    fun setAdUnitId(unitId: String?) {
        if (adViewUnitId == null){
            this.adViewUnitId = unitId
        }
    }

    fun setAdSize(size: AdSize?) {
        if (adViewSize == null) {
            this.adViewSize = size
        }
    }

    fun setAdaptiveSize(size: Int) {
        if (adViewSize == null) {
            this.adViewSize = AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(context, size)
        }
    }

    fun addListener(interstitialLoadCallback: AdMobViewLoadCallback?) {
        this.adMobViewLoadCallback = interstitialLoadCallback
    }

    fun loadAd(adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException("adRequest is not initialized yet")
        }
        if (adViewSize == null) {
            throw IllegalStateException("The ad size and ad unit ID must be set before loadAd is called.")
        }
        if (TextUtils.isEmpty(this.adViewUnitId)) {
            throw IllegalStateException("The ad unit ID must be set on Banner Unit Id before loadAd is called.")
        }

        adView = AdView(context)
        adView?.adUnitId = adViewUnitId.toString()
        adView?.adSize = adViewSize

        val adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                adMobViewLoadCallback?.onAdLoaded()
                isLoadAnimationBeforeLoadAd = false
                viewsVisibility()
            }

            override fun onAdOpened() {
                super.onAdOpened()
                adMobViewLoadCallback?.onAdOpened()
            }

            override fun onAdClicked() {
                super.onAdClicked()
                adMobViewLoadCallback?.onAdClicked()
            }

            override fun onAdClosed() {
                super.onAdClosed()
                adMobViewLoadCallback?.onAdClosed()
            }

            override fun onAdImpression() {
                super.onAdImpression()
                adMobViewLoadCallback?.onAdImpression()
            }

            override fun onAdFailedToLoad(p0: LoadAdError) {
                super.onAdFailedToLoad(p0)
                adMobViewLoadCallback?.onAdFailedToLoad(p0.message)
                isLoadAnimationBeforeLoadAd = false
                viewsVisibility()
            }
        }

        adView?.let { nonNullAdView ->
            bannerContainer?.addView(nonNullAdView)
            nonNullAdView.adListener = adListener
            nonNullAdView.loadAd(adRequest)
        }
    }

    private fun initSize(size: Int) {
        when (size) {
            BANNER -> adViewSize = AdSize.BANNER
            SMART_BANNER -> adViewSize = AdSize.SMART_BANNER
            LARGE_BANNER -> adViewSize = AdSize.LARGE_BANNER
            FULL_BANNER -> adViewSize = AdSize.FULL_BANNER
            MEDIUM_RECTANGLE -> adViewSize = AdSize.MEDIUM_RECTANGLE
        }
    }

    fun resume() {
        if (adView != null) {
            adView?.resume()
        }
    }

    fun pause() {
        if (adView != null) {
            adView?.pause()
        }
    }

    fun destroy() {
        if (adView != null) {
            adView?.destroy()
        }
    }

    companion object {
        private val TAG = AdMobBannerView::class.java.canonicalName
        private const val BANNER = 1
        private const val SMART_BANNER = 2
        private const val LARGE_BANNER = 3
        private const val FULL_BANNER = 4
        private const val MEDIUM_RECTANGLE = 5

        private val loadingAnimationType = arrayOf(
            "SquareSpinIndicator",
            "LineScalePulseOutRapidIndicator",
            "BallClipRotatePulseIndicator",
            "BallSpinFadeLoaderIndicator",
            "PacmanIndicator",
            "BallClipRotatePulseIndicator",
            "LineScalePartyIndicator"
        )
    }
}