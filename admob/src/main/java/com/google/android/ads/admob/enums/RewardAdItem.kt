package com.google.android.ads.admob.enums

data class RewardAdItem(val amount: Int, val type: String)
