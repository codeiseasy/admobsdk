package com.google.android.ads.admob.templates

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

/**
 * Denotes that the annotated element should be a float or double in the given range
 *
 *
 * Example:
 * <pre>`
 * &#64;AdTypeRange(from=0.0,to=1.0)
 * public float getAlpha() {
 * ...
 * }
`</pre> *
 */
@Retention(RetentionPolicy.CLASS)
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER,
    AnnotationTarget.VALUE_PARAMETER,
    AnnotationTarget.FIELD,
    AnnotationTarget.LOCAL_VARIABLE,
    AnnotationTarget.ANNOTATION_CLASS
)
annotation class AdMobTypeRange(
    /** Smallest value. Whether it is inclusive or not is determined
     * by [.fromInclusive]  */
    val from: Int = -1,
    /** Largest value. Whether it is inclusive or not is determined
     * by [.toInclusive]  */
    val to: Int = 7,
    /** Whether the from value is included in the range  */
    val fromInclusive: Boolean = true,
    /** Whether the to value is included in the range  */
    val toInclusive: Boolean = true
)