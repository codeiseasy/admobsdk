package com.google.android.ads.admob.adapter

import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.mediation.MediationExtrasReceiver

object MediationNetwork {
    fun request(adapter: Class<out MediationExtrasReceiver?>) : AdRequest {
        return AdRequest.Builder()
            .addNetworkExtrasBundle(adapter, Bundle())
            .build()
    }

    fun request(adapter: Class<out MediationExtrasReceiver?>, extras: Bundle) : AdRequest {
        return AdRequest.Builder()
            .addNetworkExtrasBundle(adapter, extras)
            .build()
    }

    object AdSize {
        fun adaptiveSize(context: Context?, windowManager: WindowManager, view: View?) : Int {
            var adWidth = 0

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {

                val display = context!!.display
                val outMetrics = DisplayMetrics()
                display?.getRealMetrics(outMetrics)

                val density  = context.resources.displayMetrics.density

                var adWidthPixels = view!!.width.toFloat()
                if (adWidthPixels == 0f) {
                    adWidthPixels = outMetrics.widthPixels.toFloat()
                }
                adWidth = (adWidthPixels / density).toInt()

            } else {

                val display = windowManager.defaultDisplay

                val outMetrics = DisplayMetrics()
                display.getMetrics(outMetrics)

                val density = outMetrics.density

                var adWidthPixels = view!!.width.toFloat()
                if (adWidthPixels == 0f) {
                    adWidthPixels = outMetrics.widthPixels.toFloat()
                }

                adWidth = (adWidthPixels / density).toInt()
            }

            return adWidth
        }
    }
}