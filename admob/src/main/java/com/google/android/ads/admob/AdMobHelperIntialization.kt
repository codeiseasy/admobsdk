package com.google.android.ads.admob

import android.content.Context
import com.google.android.gms.ads.MobileAds

object AdMobInitializeHelper {

    @Deprecated("This method is deprecated", ReplaceWith(
        "MobileAds.initialize(context) {}",
        "com.google.android.gms.ads.MobileAds"
    )
    )
    fun initialize(context: Context) {
        // Initialize the Mobile Ads SDK.
        MobileAds.initialize(context) {}
    }
}