package com.google.android.ads.admob.listener;

import com.google.android.ads.admob.enums.RewardAdItem;

public abstract class AdMobRewardedLoadCallback {

    public void onRewardedAdLoaded() {}

    public void onRewardedAdFailedToLoad(String error) {}

    public void onAdShowedFullScreenContent() {}

    public void onAdDismissedFullScreenContent() {}

    public void onAdFailedToShowFullScreenContent(String error) {}

    public void onAdImpression() {}

    public void onUserEarnedReward(RewardAdItem rewardItem) {}

}
