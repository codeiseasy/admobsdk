package com.google.android.ads.admob.enums

enum class AdMobSize {
    BANNER, SMART_BANNER, LARGE_BANNER, FULL_BANNER, MEDIUM_RECTANGLE, LEADERBOARD, WIDE_SKYSCRAPER, FLUID, INVALID, SEARCH
}