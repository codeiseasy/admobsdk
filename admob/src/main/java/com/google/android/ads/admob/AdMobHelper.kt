package com.google.android.ads.admob

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.LayoutRes
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.google.android.ads.R
import com.google.android.ads.admob.dialog.AdMobDialogLoading
import com.google.android.ads.admob.enums.AdMobSize
import com.google.android.ads.admob.enums.RewardAdItem
import com.google.android.ads.admob.formats.AdMobBannerView
import com.google.android.ads.admob.formats.AdMobNativeView
import com.google.android.ads.admob.listener.AdMobInterstitialLoadCallback
import com.google.android.ads.admob.listener.AdMobNativeListener
import com.google.android.ads.admob.listener.AdMobRewardedLoadCallback
import com.google.android.ads.admob.listener.AdMobViewLoadCallback
import com.google.android.ads.admob.preference.AdMobPreferences
import com.google.android.ads.admob.templates.AdMobNativeDisplaySize
import com.google.android.ads.admob.templates.AdMobNativeTemplateStyle
import com.google.android.ads.admob.util.AdMobCheckNetwork
import com.google.android.ads.admob.util.AdMobLogDebug
import com.google.android.gms.ads.*
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.gms.ads.rewarded.RewardItem
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.wang.avi.AVLoadingIndicatorView
import java.util.*


open class AdMobHelper(private val context: Context?, private val adRequest: AdRequest?) {
    private var mAdMobPreferences: AdMobPreferences? = null
    private var isDebugTestAds: Boolean = false
    private var mTestDeviceIds: List<String>? = null
    private var isEnableAds: Boolean = true
    private var isEnableBannerAd: Boolean = true
    private var isEnableInterstitialAd: Boolean = true
    private var isEnableRewardedVideoAd: Boolean = true
    private var isLoadAnimationBeforeLoadAd: Boolean = false
    private var mAnimationAccentColor: Int = android.R.color.holo_orange_dark
    private var mAnimationBeforeLoadAdHeight: Int = 100

    private var appUnitId: String? = null
    private var bannerUnitId: String? = null
    private var interstitialUnitId: String? = null
    private var rewardedVideoUnitId: String? = null
    private var nativeAdvancedUnitId: String? = null

    private var mInterstitialAd: InterstitialAd? = null
    private var mRewardedVideoAd: RewardedAd? = null
    private var mAdViewBanner: AdView? = null
    private var mAdMobNativeView: AdMobNativeView? = null
    private var mAdBannerSize: AdSize? = null

    private var mNativeTemplateStyle: AdMobNativeTemplateStyle? = null
    private var mAdLoaderBuilder: AdLoader.Builder? = null

    private var mLayoutInflater: LayoutInflater? = null
    private var mDisplayMetrics: DisplayMetrics? = null
    private var mAdMobDialogLoading: AdMobDialogLoading? = null

    private var isInterstitialLoaded: Boolean = false
    private var isRewardedVideoLoaded: Boolean = false

    private val mAdRequestNullMessage = "AdRequest class is null"
    private val mAdRequestNotInitializedMessage = "AdRequest class is not initialized yet"

    init {
        this.mAdMobPreferences = AdMobPreferences(context)
        this.mLayoutInflater = LayoutInflater.from(context)
        this.mDisplayMetrics = context!!.resources.displayMetrics
        initLoading()
    }

    private fun initLoading() {
        var loadingView = mLayoutInflater!!.inflate(R.layout.gnt_loading_layout, null)
        var avLoadingIndicatorView =
            loadingView.findViewById<AVLoadingIndicatorView>(R.id.ad_loading_view)
        avLoadingIndicatorView?.setIndicatorColor(
            ContextCompat.getColor(this.context!!, mAnimationAccentColor)
        )
        avLoadingIndicatorView?.setIndicator(
            loadingAnimationType[Random().nextInt(
                loadingAnimationType.size
            )]
        )

        var linearLayout = LinearLayout(context)
        var textView = TextView(context)

        var linearParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        linearParams.gravity = Gravity.CENTER
        linearLayout.layoutParams = linearParams
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.gravity = Gravity.CENTER

        var textParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        textParams.topMargin = 10
        textView.layoutParams = textParams
        textView.text = "AD LOADING..."
        textView.setTextColor(Color.WHITE)
        textView.setShadowLayer(2f, -1f, -1f, Color.GRAY)
        textView.textSize = 18f
        textView.isAllCaps = true

        linearLayout.addView(loadingView)
        linearLayout.addView(textView)

        this.mAdMobDialogLoading = AdMobDialogLoading.init(context)
            .cancelable(false)
            .backgroundColor(android.R.color.white)
            .view(linearLayout)
            .build()
    }

    fun build(): AdMobHelper {
        if (isDebugTestAds && mTestDeviceIds != null && mTestDeviceIds!!.isNotEmpty()) {
            val configuration =
                RequestConfiguration.Builder()
                    .setTestDeviceIds(mTestDeviceIds)
                    .build()
            MobileAds.setRequestConfiguration(configuration)
        }

        // Initialize the Mobile Ads SDK.
        MobileAds.initialize(context, OnInitializationCompleteListener {})
        return this
    }

    @Deprecated("")
    fun setAppUnitId(adUnitId: String?) {
        this.appUnitId = if (this.isDebugTestAds) {
            adTestAppUnitId
        } else {
            adUnitId
        }
    }

    fun setAdViewUnitId(adUnitId: String?) {
        this.bannerUnitId = if (this.isDebugTestAds) {
            adTestBannerId
        } else {
            adUnitId
        }
    }

    fun setInterstitialUnitId(adUnitId: String?) {
        this.interstitialUnitId = if (this.isDebugTestAds) {
            adTestInterstitialId
        } else {
            adUnitId
        }
    }

    fun setRewardedVideoUnitId(adUnitId: String?) {
        this.rewardedVideoUnitId = if (this.isDebugTestAds) {
            adTestRewardedVideoId
        } else {
            adUnitId
        }
    }

    fun setNativeAdvancedUnitId(adUnitId: String?) {
        this.nativeAdvancedUnitId = if (this.isDebugTestAds) {
            adTestNativeAdvancedId
        } else {
            adUnitId
        }
    }

    fun setAdViewSize(adSize: AdSize) {
        this.mAdBannerSize = adSize
    }

    fun setLoadAnimationBeforeLoadAd(anim: Boolean) {
        this.isLoadAnimationBeforeLoadAd = anim
    }

    fun setLoadAnimationBeforeLoadAdAccentColor(@ColorRes color: Int) {
        this.mAnimationAccentColor = color
    }

    fun setLoadAnimationBeforeLoadAdHeight(size: Int) {
        this.mAnimationBeforeLoadAdHeight = size
    }

    open fun loadAdView(adView: AdView?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadAdViewBanner(adView, null, this.adRequest)
    }

    open fun loadAdView(adView: AdView?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadAdViewBanner(adView, null, adRequest)
    }

    open fun loadAdView(adView: AdView?, viewLoadCallback: AdMobViewLoadCallback?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadAdViewBanner(adView, viewLoadCallback, this.adRequest)
    }

    open fun loadAdView(adView: AdView?, viewLoadCallback: AdMobViewLoadCallback?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadAdViewBanner(adView, viewLoadCallback, adRequest)
    }

    open fun loadAdView(layoutAd: RelativeLayout?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadAdViewBanner(layoutAd, null, this.adRequest)
    }

    open fun loadAdView(layoutAd: RelativeLayout?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadAdViewBanner(layoutAd, null, adRequest)
    }

    open fun loadAdView(layoutAd: RelativeLayout?, viewLoadCallback: AdMobViewLoadCallback?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadAdViewBanner(layoutAd, viewLoadCallback, this.adRequest)
    }

    open fun loadAdView(layoutAd: RelativeLayout?, viewLoadCallback: AdMobViewLoadCallback?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadAdViewBanner(layoutAd, viewLoadCallback, adRequest)
    }

    open fun loadAdView(layoutAd: LinearLayout?, viewLoadCallback: AdMobViewLoadCallback?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadAdViewBanner(layoutAd, viewLoadCallback, this.adRequest)
    }

    open fun loadAdView(layoutAd: LinearLayout?, viewLoadCallback: AdMobViewLoadCallback?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadAdViewBanner(layoutAd, viewLoadCallback, adRequest)
    }

    open fun loadAdView(layoutAd: LinearLayout?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadAdViewBanner(layoutAd, null, this.adRequest)
    }

    open fun loadAdView(layoutAd: LinearLayout?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadAdViewBanner(layoutAd, null, adRequest)
    }

    open fun loadAdView(layoutAd: FrameLayout?, viewLoadCallback: AdMobViewLoadCallback?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadAdViewBanner(layoutAd, viewLoadCallback, this.adRequest)
    }

    open fun loadAdView(layoutAd: FrameLayout?, viewLoadCallback: AdMobViewLoadCallback?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadAdViewBanner(layoutAd, viewLoadCallback, adRequest)
    }

    open fun loadAdView(layoutAd: FrameLayout?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadAdViewBanner(layoutAd, null, this.adRequest)
    }

    open fun loadAdView(layoutAd: FrameLayout?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadAdViewBanner(layoutAd, null, adRequest)
    }

    open fun loadAdView(layoutAd: ConstraintLayout?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadAdViewBanner(layoutAd, null, this.adRequest)
    }

    open fun loadAdView(layoutAd: ConstraintLayout?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadAdViewBanner(layoutAd, null, adRequest)
    }

    open fun loadAdView(layoutAd: ConstraintLayout?, viewLoadCallback: AdMobViewLoadCallback?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadAdViewBanner(layoutAd, viewLoadCallback, this.adRequest)
    }

    open fun loadAdView(layoutAd: ConstraintLayout?, viewLoadCallback: AdMobViewLoadCallback?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadAdViewBanner(layoutAd, viewLoadCallback, adRequest)
    }

    open fun loadAdView(layoutAd: CardView?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadAdViewBanner(layoutAd, null, this.adRequest)
    }

    open fun loadAdView(layoutAd: CardView?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadAdViewBanner(layoutAd, null, adRequest)
    }

    open fun loadAdView(layoutAd: CardView?, viewLoadCallback: AdMobViewLoadCallback?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadAdViewBanner(layoutAd, viewLoadCallback, this.adRequest)
    }

    open fun loadAdView(layoutAd: CardView?, viewLoadCallback: AdMobViewLoadCallback?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadAdViewBanner(layoutAd, viewLoadCallback, adRequest)
    }

    open fun loadAdView(layoutAd: AdMobBannerView?, viewLoadCallback: AdMobViewLoadCallback?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadAdViewBanner(layoutAd, viewLoadCallback, this.adRequest)
    }

    open fun loadAdView(layoutAd: AdMobBannerView?, viewLoadCallback: AdMobViewLoadCallback?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadAdViewBanner(layoutAd, viewLoadCallback, adRequest)
    }

    private fun loadAdViewBanner(adView: AdView?, viewLoadCallback: AdMobViewLoadCallback?, adRequest: AdRequest?) {
        if (isEnableAds) {
            if (isEnableBannerAd) {
                this.loadAdViewIfEnabled(adView, viewLoadCallback, adRequest)
            } else {
                viewLoadCallback?.onAdFailedToLoad("Nothing(BANNER)")
            }
        } else {
            if (isEnableBannerAd) {
                this.loadAdViewIfEnabled(adView, viewLoadCallback, adRequest)
            } else {
                viewLoadCallback?.onAdFailedToLoad("Nothing(BANNER)")
            }
        }
    }

    private fun loadAdViewBanner(layoutAd: ViewGroup?, viewLoadCallback: AdMobViewLoadCallback?, adRequest: AdRequest?) {
        if (TextUtils.isEmpty(this.bannerUnitId)) {
            throw IllegalStateException("The ad unit ID must be set on AdViewUnitId before loadAd is called.")
        }
        if (this.mAdBannerSize == null) {
            throw IllegalStateException("ad size $initError")
        }
        if (isEnableAds) {
            if (isEnableBannerAd) {
                this.loadAdViewIfEnabled(layoutAd, viewLoadCallback, adRequest)
            } else {
                viewLoadCallback?.onAdFailedToLoad("Nothing(BANNER)")
            }
        } else {
            if (isEnableBannerAd) {
                this.loadAdViewIfEnabled(layoutAd, viewLoadCallback, adRequest)
            } else {
                viewLoadCallback?.onAdFailedToLoad("Nothing(BANNER)")
            }
        }
    }

    private fun loadAdViewBanner(
        layoutAd: AdMobBannerView?,
        viewLoadCallback: AdMobViewLoadCallback?,
        adRequest: AdRequest?
    ) {
        if (TextUtils.isEmpty(this.bannerUnitId)) {
            throw IllegalStateException("The ad unit ID must be set on banner Unit Id before loadAd is called.")
        }
        if (this.mAdBannerSize == null) {
            throw IllegalStateException("ad size $initError")
        }
        if (isEnableAds) {
            if (isEnableBannerAd) {
                this.loadAdViewIfEnabled(layoutAd, viewLoadCallback, adRequest)
            } else {
                viewLoadCallback?.onAdFailedToLoad("Nothing(BANNER)")
            }
        } else {
            if (isEnableBannerAd) {
                this.loadAdViewIfEnabled(layoutAd, viewLoadCallback, adRequest)
            } else {
                viewLoadCallback?.onAdFailedToLoad("Nothing(BANNER)")
            }
        }
    }

    private fun loadAdViewIfEnabled(
        layoutAd: ViewGroup?,
        viewLoadCallback: AdMobViewLoadCallback?,
        adRequest: AdRequest?
    ) {
        var adContainer = layoutAd

        mAdViewBanner = AdView(context)
        mAdViewBanner?.adUnitId = bannerUnitId
        mAdViewBanner?.adSize = mAdBannerSize

        var params = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        params.addRule(RelativeLayout.CENTER_IN_PARENT)
        mAdViewBanner?.layoutParams = params
        mAdViewBanner?.id = R.id.gnt_view_banner_id
        mAdViewBanner?.let { nonNullAdView ->
            adContainer?.removeView(nonNullAdView)
            adContainer?.addView(nonNullAdView)
            nonNullAdView.loadAd(adRequest)
            adViewListener(viewLoadCallback)
        }
    }

    private fun loadAdViewIfEnabled(
        adView: AdMobBannerView?,
        viewLoadCallback: AdMobViewLoadCallback?,
        adRequest: AdRequest?
    ) {
        adView?.setAdUnitId(this.bannerUnitId)
        adView?.setAdSize(this.mAdBannerSize)
        adView?.addListener(viewLoadCallback)
        adView?.loadAd(adRequest)
    }

    private fun loadAdViewIfEnabled(adView: AdView, viewLoadCallback: AdMobViewLoadCallback?, adRequest: AdRequest?) {
        this.mAdViewBanner = adView
        this.mAdViewBanner?.adSize = this.mAdBannerSize
        this.mAdViewBanner?.loadAd(adRequest)
        this.adViewListener(viewLoadCallback)
    }

    open fun loadInterstitialAd(activity: Activity) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadInterstitialAd(activity, null, this.adRequest)
    }

    open fun loadInterstitialAd(activity: Activity, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadInterstitialAd(activity, null, adRequest)
    }

    open fun loadInterstitialAd(
        activity: Activity,
        interstitialLoadCallback: AdMobInterstitialLoadCallback?, adRequest: AdRequest?
    ) {
        if (TextUtils.isEmpty(this.interstitialUnitId)) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialAd before loadAd is called.")
        }
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        if (isEnableAds) {
            if (isEnableInterstitialAd) {
                loadInterstitialAdIfEnabled(activity, interstitialLoadCallback, adRequest)
            } else {
                interstitialLoadCallback?.onAdFailedToLoad("Nothing")
            }
        } else {
            if (isEnableInterstitialAd) {
                loadInterstitialAdIfEnabled(activity, interstitialLoadCallback, adRequest)
            } else {
                interstitialLoadCallback?.onAdFailedToLoad("Nothing")
            }
        }
    }

    private fun loadInterstitialAdIfEnabled(activity: Activity, interstitialLoadCallback: AdMobInterstitialLoadCallback?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.interstitialAdListener(activity, interstitialLoadCallback, this.adRequest)
    }

    private fun loadInterstitialAdIfEnabled(activity: Activity, interstitialLoadCallback: AdMobInterstitialLoadCallback?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.interstitialAdListener(activity, interstitialLoadCallback, adRequest)
    }

    open fun loadAdRewardedVideo(activity: Activity) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadAdRewardedVideo(activity, null, this.adRequest)
    }

    open fun loadAdRewardedVideo(activity: Activity, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadAdRewardedVideo(activity, null, adRequest)
    }

    open fun loadAdRewardedVideo(
        activity: Activity,
        rewardedLoadCallback: AdMobRewardedLoadCallback?, adRequest: AdRequest?
    ) {
        if (TextUtils.isEmpty(this.rewardedVideoUnitId)) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        if (isEnableAds) {
            if (isEnableRewardedVideoAd) {
                this.loadAdRewardedVideoIfEnabled(activity, rewardedLoadCallback, adRequest)
            } else {
                rewardedLoadCallback?.onRewardedAdFailedToLoad("Enabled")
            }
        } else {
            if (isEnableRewardedVideoAd) {
                this.loadAdRewardedVideoIfEnabled(activity, rewardedLoadCallback, adRequest)
            } else {
                rewardedLoadCallback?.onRewardedAdFailedToLoad("Disabled")
            }
        }
    }

    private fun loadAdRewardedVideoIfEnabled(
        activity: Activity,
        rewardedLoadCallback: AdMobRewardedLoadCallback?, adRequest: AdRequest?
    ) {
        this.rewardedVideoListener(activity, rewardedLoadCallback, adRequest)
    }

    open fun loadInterstitialAfterEndCount(activity: Activity, view: View?, count: Int) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadInterstitialAfterEndCount(activity, view!!.id.toString(), count, null, this.adRequest)
    }

    open fun loadInterstitialAfterEndCount(activity: Activity, view: View?, count: Int, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadInterstitialAfterEndCount(activity, view!!.id.toString(), count, null, adRequest)
    }

    open fun loadInterstitialAfterEndCount(activity: Activity, key: String?, count: Int) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadInterstitialAfterEndCount(activity, key.toString(), count, null, this.adRequest)
    }

    open fun loadInterstitialAfterEndCount(activity: Activity, key: String?, count: Int, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadInterstitialAfterEndCount(activity, key.toString(), count, null, adRequest)
    }

    open fun loadInterstitialAfterEndCount(activity: Activity, view: View?, count: Int, interstitialLoadCallback: AdMobInterstitialLoadCallback?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadInterstitialAfterEndCount(activity, view!!.id.toString(), count, interstitialLoadCallback, this.adRequest)
    }

    open fun loadInterstitialAfterEndCount(activity: Activity, key: String?, count: Int, interstitialLoadCallback: AdMobInterstitialLoadCallback?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadInterstitialAfterEndCount(activity, key.toString(), count, interstitialLoadCallback, this.adRequest)
    }

    open fun loadInterstitialAfterEndCount(activity: Activity, view: View?, count: Int, interstitialLoadCallback: AdMobInterstitialLoadCallback?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadInterstitialAfterEndCount(activity, view!!.id.toString(), count, interstitialLoadCallback, adRequest)
    }

    open fun loadInterstitialAfterEndCount(activity: Activity, key: String?, count: Int, interstitialLoadCallback: AdMobInterstitialLoadCallback?, adRequest: AdRequest?) {
        Log.d("ADMOB", "AdMobHelper init(${this.interstitialUnitId})")
        if (this.interstitialUnitId.isNullOrEmpty()) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialUnitId before loadAd is called.")
        }
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        if (isEnableAds) {
            if (isEnableInterstitialAd) {
                loadInterstitialAfterEndCountIfEnabled(
                    activity,
                    key,
                    count,
                    interstitialLoadCallback,
                    adRequest
                )
            } else {
                interstitialLoadCallback?.onAdDismissedFullScreenContent()
            }
        } else {
            if (isEnableInterstitialAd) {
                loadInterstitialAfterEndCountIfEnabled(
                    activity,
                    key,
                    count,
                    interstitialLoadCallback,
                    adRequest
                )
            } else {
                interstitialLoadCallback?.onAdDismissedFullScreenContent()
            }
        }
    }

    private fun loadInterstitialAfterEndCountIfEnabled(activity: Activity, key: String?, count: Int, interstitialLoadCallback: AdMobInterstitialLoadCallback?, adRequest: AdRequest?) {
        var totalCount = doCountInterstitialAd(key)
        if (AdMobCheckNetwork.getInstance(this.context).isOnline) {
            if (this.mAdMobPreferences!!.getAdCountPref(key) == count) {
                this.mAdMobPreferences?.setAdCountPref(key, 1)
                this.interstitialAdListener(activity, interstitialLoadCallback, adRequest)
            } else {
                this.mAdMobPreferences?.setAdCountPref(key, totalCount)
                interstitialLoadCallback?.onAdDismissedFullScreenContent()
            }
        } else {
            interstitialLoadCallback?.onAdDismissedFullScreenContent()
        }
        Log.d("LOAD_AD_AFTER_COUNTDOWN", totalCount.toString())
    }

    open fun loadRewardedVideoAfterEndCount(activity: Activity, view: View?, count: Int) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadRewardedVideoAfterEndCount(activity, view!!.id.toString(), count, null, this.adRequest)
    }

    open fun loadRewardedVideoAfterEndCount(activity: Activity, view: View?, count: Int, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadRewardedVideoAfterEndCount(activity, view!!.id.toString(), count, null, adRequest)
    }

    open fun loadRewardedVideoAfterEndCount(activity: Activity, key: String?, count: Int) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadRewardedVideoAfterEndCount(activity, key.toString(), count, null, this.adRequest)
    }

    open fun loadRewardedVideoAfterEndCount(activity: Activity, key: String?, count: Int, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadRewardedVideoAfterEndCount(activity, key.toString(), count, null, adRequest)
    }

    open fun loadRewardedVideoAfterEndCount(
        activity: Activity,
        view: View?,
        count: Int,
        rewardedLoadCallback: AdMobRewardedLoadCallback?
    ) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadRewardedVideoAfterEndCount(activity, view!!.id.toString(), count, rewardedLoadCallback, this.adRequest)
    }

    open fun loadRewardedVideoAfterEndCount(
        activity: Activity,
        view: View?,
        count: Int,
        rewardedLoadCallback: AdMobRewardedLoadCallback?, adRequest: AdRequest?
    ) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadRewardedVideoAfterEndCount(activity, view!!.id.toString(), count, rewardedLoadCallback, adRequest)
    }

    open fun loadRewardedVideoAfterEndCount(
        activity: Activity,
        key: String,
        count: Int,
        rewardedLoadCallback: AdMobRewardedLoadCallback?, adRequest: AdRequest?
    ) {
        if (this.rewardedVideoUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on rewarded video unit id before loadAd is called.")
        }
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        if (isEnableAds) {
            if (isEnableRewardedVideoAd) {
                loadRewardedVideoAfterEndCountIfEnabled(activity, key, count, rewardedLoadCallback, adRequest)
            } else {
                rewardedLoadCallback?.onAdDismissedFullScreenContent()
            }
        } else {
            if (isEnableRewardedVideoAd) {
                loadRewardedVideoAfterEndCountIfEnabled(activity, key, count, rewardedLoadCallback, adRequest)
            } else {
                rewardedLoadCallback?.onAdDismissedFullScreenContent()
            }
        }
    }

    private fun loadRewardedVideoAfterEndCountIfEnabled(
        activity: Activity,
        key: String,
        count: Int,
        rewardedLoadCallback: AdMobRewardedLoadCallback?, adRequest: AdRequest?
    ) {
        var totalCount = doCountRewardedVideo(key)
        if (AdMobCheckNetwork.getInstance(this.context).isOnline) {
            if (this.mAdMobPreferences!!.getAdCountPref(key) == count) {
                this.mAdMobPreferences?.setAdCountPref(key, 1)
                this.rewardedVideoListener(activity, rewardedLoadCallback, adRequest)
            } else {
                this.mAdMobPreferences?.setAdCountPref(key, totalCount)
                rewardedLoadCallback?.onAdDismissedFullScreenContent()
            }
        } else {
            rewardedLoadCallback?.onAdDismissedFullScreenContent()
        }
        AdMobLogDebug.d("LOAD_AD_AFTER_COUNTDOWN", totalCount.toString())
    }

    private fun doCountInterstitialAd(key: String?): Int {
        return this.mAdMobPreferences!!.getAdCountPref(key) + 1
    }

    private fun doCountRewardedVideo(key: String?): Int {
        return this.mAdMobPreferences!!.getAdCountPref(key) + 1
    }

    private fun adViewListener(viewLoadCallback: AdMobViewLoadCallback?) {
        this.mAdViewBanner?.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                viewLoadCallback?.onAdLoaded()
                AdMobLogDebug.d(TAG, "[loadCustomAdView(){\nonAdLoaded()}]")
            }

            override fun onAdOpened() {
                super.onAdOpened()
                viewLoadCallback?.onAdOpened()
                AdMobLogDebug.d(TAG, "[loadCustomAdView(){\nonAdOpened()}]")
            }

            override fun onAdClicked() {
                super.onAdClicked()
                viewLoadCallback?.onAdClicked()
                AdMobLogDebug.d(TAG, "[loadCustomAdView(){\nonAdClicked()}]")
            }

            override fun onAdClosed() {
                super.onAdClosed()
                viewLoadCallback?.onAdClosed()
                AdMobLogDebug.d(TAG, "[loadCustomAdView(){\nonAdClosed()}]")
            }

            override fun onAdImpression() {
                super.onAdImpression()
                viewLoadCallback?.onAdImpression()
                AdMobLogDebug.d(TAG, "[loadCustomAdView(){\nonAdImpression()}]")
            }

            override fun onAdFailedToLoad(p0: LoadAdError) {
                super.onAdFailedToLoad(p0)
                AdMobLogDebug.d(TAG, "[loadCustomAdView(){\nonAdFailedToLoad(${p0.message})}]")
            }
        }
    }

    private fun showLoading() {
        if (isLoadAnimationBeforeLoadAd) {
            this.mAdMobDialogLoading?.show()
        }
    }

    private fun hideLoading() {
        if (isLoadAnimationBeforeLoadAd) {
            this.mAdMobDialogLoading?.hide()
        }
    }

    private fun interstitialAdListener(
        activity: Activity,
        interstitialLoadCallback: AdMobInterstitialLoadCallback?, adRequest: AdRequest?
    ) {
        showLoading()
        requestInterstitialAd(activity, interstitialLoadCallback, adRequest)
    }

    private fun requestInterstitialAd(
        activity: Activity,
        interstitialLoadCallback: AdMobInterstitialLoadCallback?, adRequest: AdRequest?
    ) {
        showInterstitialAd(activity)

        InterstitialAd.load(context, interstitialUnitId.toString(), adRequest, object :
            InterstitialAdLoadCallback() {
            override fun onAdLoaded(ad: InterstitialAd) {
                super.onAdLoaded(ad)
                mInterstitialAd = ad
                showInterstitialAd(activity)
                interstitialLoadCallback?.onAdLoaded()

                mInterstitialAd?.fullScreenContentCallback = object : FullScreenContentCallback() {
                    override fun onAdShowedFullScreenContent() {
                        super.onAdShowedFullScreenContent()
                        interstitialLoadCallback?.onAdShowedFullScreenContent()
                    }

                    override fun onAdDismissedFullScreenContent() {
                        super.onAdDismissedFullScreenContent()
                        interstitialLoadCallback?.onAdDismissedFullScreenContent()
                    }

                    override fun onAdFailedToShowFullScreenContent(p0: AdError) {
                        super.onAdFailedToShowFullScreenContent(p0)
                        interstitialLoadCallback?.onAdFailedToShowFullScreenContent(p0.message)
                    }

                    override fun onAdImpression() {
                        super.onAdImpression()
                        interstitialLoadCallback?.onAdImpression()
                    }
                }
                AdMobLogDebug.d(TAG, "[loadInterstitialAd(){\nonAdLoaded()}]")
                hideLoading()
            }

            override fun onAdFailedToLoad(error: LoadAdError) {
                super.onAdFailedToLoad(error)
                mInterstitialAd = null
                interstitialLoadCallback?.onAdFailedToLoad(error.message)
                AdMobLogDebug.d(TAG, "[loadInterstitialAd(){\nonAdFailedToLoad(${error.message})}]")
                hideLoading()
            }
        })
    }

    private fun showInterstitialAd(activity: Activity) {
        if (mInterstitialAd != null) {
            isInterstitialLoaded = true
            mInterstitialAd?.show(activity)
            return
        }
    }

    private fun rewardedVideoListener(
        activity: Activity,
        rewardedLoadCallback: AdMobRewardedLoadCallback?, adRequest: AdRequest?
    ) {
        showLoading()
        requestRewardedVideoAd(activity, rewardedLoadCallback, adRequest)
    }

    private fun requestRewardedVideoAd(
        activity: Activity,
        rewardedLoadCallback: AdMobRewardedLoadCallback?, adRequest: AdRequest?
    ) {
        showRewardedAd(activity, rewardedLoadCallback)
        RewardedAd.load(
            context,
            rewardedVideoUnitId.toString(),
            adRequest,
            object : RewardedAdLoadCallback() {
                override fun onAdLoaded(ad: RewardedAd) {
                    super.onAdLoaded(ad)
                    mRewardedVideoAd = ad
                    rewardedLoadCallback?.onRewardedAdLoaded()
                    showRewardedAd(activity, rewardedLoadCallback)

                    mRewardedVideoAd?.fullScreenContentCallback =
                        object : FullScreenContentCallback() {
                            override fun onAdShowedFullScreenContent() {
                                super.onAdShowedFullScreenContent()
                                rewardedLoadCallback?.onAdShowedFullScreenContent()
                            }

                            override fun onAdDismissedFullScreenContent() {
                                super.onAdDismissedFullScreenContent()
                                rewardedLoadCallback?.onAdDismissedFullScreenContent()
                            }

                            override fun onAdFailedToShowFullScreenContent(error: AdError) {
                                super.onAdFailedToShowFullScreenContent(error)
                                rewardedLoadCallback?.onAdFailedToShowFullScreenContent(error.message)
                            }

                            override fun onAdImpression() {
                                super.onAdImpression()
                                rewardedLoadCallback?.onAdImpression()
                            }
                        }
                    hideLoading()
                }

                override fun onAdFailedToLoad(p0: LoadAdError) {
                    super.onAdFailedToLoad(p0)
                    rewardedLoadCallback?.onRewardedAdFailedToLoad(p0.message)
                    hideLoading()
                }
            })
    }

    private fun showRewardedAd(
        activity: Activity,
        rewardedLoadCallback: AdMobRewardedLoadCallback?
    ) {
        if (mRewardedVideoAd != null) {
            isRewardedVideoLoaded = true
            mRewardedVideoAd?.show(activity
            ) { rewardItem ->
                rewardedLoadCallback?.onUserEarnedReward(
                    RewardAdItem(
                        rewardItem.amount,
                        rewardItem.type
                    )
                )
            }
            return
        }
    }

    fun loadNativeAd(unitAdView: ViewGroup) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadUnifiedNativeAd(unitAdView, AdMobNativeDisplaySize.MEDIUM, null, this.adRequest)
    }

    fun loadNativeAd(unitAdView: ViewGroup, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadUnifiedNativeAd(unitAdView, AdMobNativeDisplaySize.MEDIUM, null, adRequest)
    }

    fun loadNativeAd(unitAdView: ViewGroup, adMobNativeListener: AdMobNativeListener?) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadUnifiedNativeAd(unitAdView, AdMobNativeDisplaySize.MEDIUM, adMobNativeListener, this.adRequest)
    }

    fun loadNativeAd(unitAdView: ViewGroup, adMobNativeListener: AdMobNativeListener?, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadUnifiedNativeAd(unitAdView, AdMobNativeDisplaySize.MEDIUM, adMobNativeListener, adRequest)
    }

    fun loadNativeAd(unitAdView: ViewGroup, displaySize: AdMobNativeDisplaySize) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadUnifiedNativeAd(unitAdView, displaySize, null, this.adRequest)
    }

    fun loadNativeAd(unitAdView: ViewGroup, displaySize: AdMobNativeDisplaySize, adRequest: AdRequest?) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadUnifiedNativeAd(unitAdView, displaySize, null, adRequest)
    }

    fun loadNativeAd(
        unitAdView: ViewGroup,
        displaySize: AdMobNativeDisplaySize,
        adMobNativeListener: AdMobNativeListener?
    ) {
        if (this.adRequest == null) {
            throw IllegalStateException(mAdRequestNotInitializedMessage)
        }
        this.loadUnifiedNativeAd(unitAdView, displaySize, adMobNativeListener, this.adRequest)
    }

    fun loadNativeAd(
        unitAdView: ViewGroup,
        displaySize: AdMobNativeDisplaySize,
        adMobNativeListener: AdMobNativeListener?,
        adRequest: AdRequest?
    ) {
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        this.loadUnifiedNativeAd(unitAdView, displaySize, adMobNativeListener, adRequest)
    }

    private fun loadUnifiedNativeAd(
        unitAdView: ViewGroup,
        displaySize: AdMobNativeDisplaySize,
        adMobNativeListener: AdMobNativeListener?, adRequest: AdRequest?
    ) {
        if (adRequest != null) {
            when (displaySize) {
                AdMobNativeDisplaySize.SMALL -> this.refreshNativeAd(
                    unitAdView,
                    R.layout.gnt_small_template_view, // ad_small_unified_native
                    adMobNativeListener,
                    adRequest
                )
                AdMobNativeDisplaySize.MEDIUM -> this.refreshNativeAd(
                    unitAdView,
                    R.layout.gnt_medium_template_view, // ad_medium_unified_native
                    adMobNativeListener,
                    adRequest
                )
                AdMobNativeDisplaySize.LARGE -> this.refreshNativeAd(
                    unitAdView,
                    R.layout.gnt_large_template_view,
                    adMobNativeListener,
                    adRequest
                )
            }
        }
    }

    private fun refreshNativeAd(
        unitAd: ViewGroup,
        @LayoutRes layoutRes: Int,
        adMobNativeListener: AdMobNativeListener?,
        adRequest: AdRequest
    ) {
        if (TextUtils.isEmpty(this.nativeAdvancedUnitId)) {
            throw IllegalStateException("The ad unit ID must be set on NativeAdvancedUnitId before loadAd is called.")
        }
        if (adRequest == null) {
            throw IllegalStateException(mAdRequestNullMessage)
        }
        AdMobLogDebug.d("TEST_MODE", isDebugTestAds.toString())
        if (isEnableAds) {
            var colorDrawable = ColorDrawable(Color.parseColor("#FFFFFF"))
            if (mNativeTemplateStyle == null) {
                this.mNativeTemplateStyle = AdMobNativeTemplateStyle.Builder()
                    .withMainBackgroundColor(colorDrawable)
                    .build()
            }

            val adMobNativeView: AdMobNativeView = unitAd as AdMobNativeView
            adMobNativeView.unitId = this.nativeAdvancedUnitId.toString()
            adMobNativeView.templateRes = layoutRes
            adMobNativeView.templateStyle = mNativeTemplateStyle
            adMobNativeView.loadAd(adRequest)
            adMobNativeView.adMobNativeListener = object : AdMobNativeListener() {
                override fun onAdLoaded() {
                    super.onAdLoaded()
                    adMobNativeListener?.onAdLoaded()
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    adMobNativeListener?.onAdOpened()
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    adMobNativeListener?.onAdClosed()
                }

                override fun onAdFailedToLoad(error: String?) {
                    super.onAdFailedToLoad(error)
                    adMobNativeListener?.onAdFailedToLoad(error)
                }

                override fun onAdClicked() {
                    super.onAdClicked()
                    adMobNativeListener?.onAdClicked()
                }

                override fun onAdImpression() {
                    super.onAdImpression()
                    adMobNativeListener?.onAdImpression()
                }

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    adMobNativeListener?.onAdLeftApplication()
                }
            }
        }
    }

    // onResume Ads
    fun resume() {
        if (mAdViewBanner != null) {
            mAdViewBanner?.resume()
        }
    }

    // ooPause Ads
    fun pause() {
        if (mAdViewBanner != null) {
            mAdViewBanner?.pause()
        }
    }

    // onDestroy Ads
    fun destroy() {
        if (mAdMobNativeView != null) {
            mAdMobNativeView?.destroyNativeAd()
        }
        if (mAdViewBanner != null) {
            mAdViewBanner?.destroy()
        }
    }

    //TODO  Builder class
    class Builder {
        private var context: Context? = null
        private var adSize: AdSize = AdSize.BANNER
        private var appUnitId: String? = null
        private var adViewUnitId: String? = null
        private var interstitialUnitId: String? = null
        private var rewardedVideoUnitId: String? = null
        private var nativeAdvancedUnitId: String? = null
        private var animationAccentColor: Int = android.R.color.holo_orange_dark
        private var animationBeforeLoadAdHeight: Int = 100
        private var adRequest: AdRequest? = null
        private var isDebugTestAds: Boolean = false
        private var testDeviceIds: List<String>? = null
        private var isEnableAds: Boolean = true
        private var isEnableBannerAd: Boolean = true
        private var isEnableInterstitialAd: Boolean = true
        private var isEnableRewardedVideoAd: Boolean = true
        private var isLoadAnimationBeforeLoadAd: Boolean = false
        private var nativeAdvancedTemplateStyles: AdMobNativeTemplateStyle? = null

        fun setContext(context: Context?): Builder {
            this.context = context
            return this
        }

        @Deprecated("")
        fun setAppUnitId(unitId: String): Builder {
            this.appUnitId = unitId
            return this
        }

        fun setAdViewUnitId(unitId: String): Builder {
            this.adViewUnitId = unitId
            return this
        }

        fun setInterstitialUnitId(unitId: String): Builder {
            this.interstitialUnitId = unitId
            return this
        }

        fun setRewardedVideoUnitId(unitId: String): Builder {
            this.rewardedVideoUnitId = unitId
            return this
        }

        fun setNativeAdvancedUnitId(unitId: String): Builder {
            this.nativeAdvancedUnitId = unitId
            return this
        }

        fun setAdViewSize(size: AdMobSize): Builder {
            checkAdSize(size)
            return this
        }

        fun setAdViewAdaptiveSize(adWidth: Int): Builder {
            this.adSize = AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(context, adWidth)
            return this
        }

        private fun checkAdSize(size: AdMobSize) {
            this.adSize = when (size) {
                AdMobSize.BANNER -> AdSize.BANNER
                AdMobSize.SMART_BANNER -> AdSize.SMART_BANNER
                AdMobSize.LARGE_BANNER -> AdSize.LARGE_BANNER
                AdMobSize.FULL_BANNER -> AdSize.FULL_BANNER
                AdMobSize.MEDIUM_RECTANGLE -> AdSize.MEDIUM_RECTANGLE
                AdMobSize.LEADERBOARD -> AdSize.LEADERBOARD
                AdMobSize.WIDE_SKYSCRAPER -> AdSize.WIDE_SKYSCRAPER
                AdMobSize.FLUID -> AdSize.FLUID
                AdMobSize.INVALID -> AdSize.INVALID
                AdMobSize.SEARCH -> AdSize.SEARCH
            }
        }

        fun setAdRequest(request: AdRequest?): Builder {
            this.adRequest = request
            return this
        }

        fun setDebugTestAds(debug: Boolean): Builder {
            this.isDebugTestAds = debug
            return this
        }

        fun setTestDeviceIds(params: List<String>?): Builder {
            this.testDeviceIds = params
            return this
        }

        fun setLoadAnimationBeforeLoadAd(anim: Boolean): Builder {
            this.isLoadAnimationBeforeLoadAd = anim
            return this
        }

        fun setLoadAnimationBeforeLoadAdAccentColor(@ColorRes color: Int): Builder {
            this.animationAccentColor = color
            return this
        }

        fun setLoadAnimationBeforeLoadAdHeight(size: Int): Builder {
            this.animationBeforeLoadAdHeight = size
            return this
        }

        fun setEnableAds(enable: Boolean): Builder {
            this.isEnableAds = enable
            return this
        }

        fun setEnableBannerAd(enable: Boolean): Builder {
            this.isEnableBannerAd = enable
            this.isEnableAds = enable
            return this
        }

        fun setEnableInterstitialAd(enable: Boolean): Builder {
            this.isEnableInterstitialAd = enable
            this.isEnableAds = enable
            return this
        }

        fun setEnableRewardedVideoAd(enable: Boolean): Builder {
            this.isEnableRewardedVideoAd = enable
            this.isEnableAds = enable
            return this
        }

        fun setNativeAdvancedStyles(styles: AdMobNativeTemplateStyle): Builder {
            this.nativeAdvancedTemplateStyles = styles
            return this
        }

        fun build(): AdMobHelper {
            var advertiseHandler = AdMobHelper(this.context, adRequest)
            advertiseHandler.isDebugTestAds = this.isDebugTestAds
            advertiseHandler.mTestDeviceIds = this.testDeviceIds
            advertiseHandler.isEnableAds = this.isEnableAds
            advertiseHandler.isEnableBannerAd = this.isEnableBannerAd
            advertiseHandler.isEnableInterstitialAd = this.isEnableInterstitialAd
            advertiseHandler.isEnableRewardedVideoAd = this.isEnableRewardedVideoAd
            advertiseHandler.mNativeTemplateStyle = this.nativeAdvancedTemplateStyles

            advertiseHandler.setAdViewSize(this.adSize)
            advertiseHandler.setLoadAnimationBeforeLoadAd(this.isLoadAnimationBeforeLoadAd)
            advertiseHandler.setLoadAnimationBeforeLoadAdAccentColor(this.animationAccentColor)
            advertiseHandler.setLoadAnimationBeforeLoadAdHeight(this.animationBeforeLoadAdHeight)

            advertiseHandler.setAppUnitId(this.appUnitId)
            advertiseHandler.setAdViewUnitId(this.adViewUnitId)
            advertiseHandler.setInterstitialUnitId(this.interstitialUnitId)
            advertiseHandler.setRewardedVideoUnitId(this.rewardedVideoUnitId)
            advertiseHandler.setNativeAdvancedUnitId(this.nativeAdvancedUnitId)

            advertiseHandler.build()
            return advertiseHandler
        }
    }

    companion object {
        private var TAG = AdMobHelper::class.java.name
        private const val initError = "is not initialized yet"
        private const val adTestAppUnitId = "ca-app-pub-3940256099942544~3347511713"
        private const val adTestBannerId = "ca-app-pub-3940256099942544/6300978111"
        private const val adTestInterstitialId = "ca-app-pub-3940256099942544/1033173712"
        private const val adTestRewardedVideoId = "ca-app-pub-3940256099942544/5224354917"
        private const val adTestNativeAdvancedId = "ca-app-pub-3940256099942544/2247696110"
        private const val adTestNativeAdvancedVideoId = "ca-app-pub-3940256099942544/1044960115"

        private val loadingAnimationType = arrayOf(
            "SquareSpinIndicator",
            "LineScalePulseOutRapidIndicator",
            "BallClipRotatePulseIndicator",
            "BallSpinFadeLoaderIndicator",
            "PacmanIndicator",
            "BallClipRotatePulseIndicator",
            "LineScalePartyIndicator"
        )

        var adMobHelper: AdMobHelper? = null

        @Synchronized
        fun getInstance(context: Context?, adRequest: AdRequest): AdMobHelper? {
            synchronized(AdMobHelper::class.java) {
                if (adMobHelper == null)
                    return AdMobHelper(context, adRequest)
            }
            return adMobHelper
        }
    }
}