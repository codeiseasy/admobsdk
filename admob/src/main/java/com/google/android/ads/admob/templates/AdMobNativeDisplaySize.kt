package com.google.android.ads.admob.templates

enum class AdMobNativeDisplaySize {
    SMALL,
    MEDIUM,
    LARGE
}