package com.codeiseasydev.admobhelper

import android.content.Intent
import android.os.Bundle
import com.google.android.ads.admob.util.AdMobLogDebug
import com.google.android.ads.consent.ConsentStatus
import com.google.android.ads.consent.listener.interfaces.UserConsentEventListener
import com.google.android.ads.consent.prefs.UserConsentPreferences


class SplashActivity : UserConsentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (UserConsentPreferences.getInstance(this@SplashActivity).isUserConfirmAgreement()){
             goToMain()
        } else {
            checkUserConsent(object : UserConsentEventListener {
                override fun onResult(
                    consentStatus: ConsentStatus,
                    isRequestLocationInEeaOrUnknown: Boolean
                ) {
                    AdMobLogDebug.d("CHECKING_CONSENT", "onResult(${consentStatus.name})")
                    if (isRequestLocationInEeaOrUnknown) {
                        UserConsentPreferences.getInstance(this@SplashActivity).setUserConfirmAgreement(true)
                        goToMain()
                    }
                }

                override fun onFailed(reason: String) {
                    AdMobLogDebug.d("CHECKING_CONSENT", "onFailed($reason)")
                    UserConsentPreferences.getInstance(this@SplashActivity).setUserConfirmAgreement(true)
                    goToMain()
                }
            })
        }
    }

    fun goToMain(){
        startActivity(Intent(this@SplashActivity, AdMobActivity::class.java))
        finish()
    }
}