package com.codeiseasydev.admobhelper

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem

import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.applovin.mediation.AppLovinExtras
import com.applovin.mediation.ApplovinAdapter
import com.codeiseasydev.admobhelper.Constants.privacyPolicy
import com.codeiseasydev.admobhelper.Constants.publisherId
import com.google.ads.mediation.facebook.FacebookAdapter
import com.google.ads.mediation.facebook.FacebookExtras
import com.google.android.ads.admob.AdMobHelper
import com.google.android.ads.admob.adapter.MediationNetwork
import com.google.android.ads.admob.enums.RewardAdItem
import com.google.android.ads.admob.formats.AdMobBannerView
import com.google.android.ads.admob.formats.AdMobNativeView
import com.google.android.ads.admob.listener.AdMobInterstitialLoadCallback
import com.google.android.ads.admob.listener.AdMobRewardedLoadCallback
import com.google.android.ads.admob.listener.AdMobViewLoadCallback
import com.google.android.ads.admob.templates.AdMobNativeTemplateStyle
import com.google.android.ads.admob.util.AdMobLogDebug
import com.google.android.ads.consent.UserConsentSdk
import com.google.android.ads.consent.ui.UserConsentSettingsActivity
//import com.google.android.ads.mediationtestsuite.MediationTestSuite
import com.google.android.gms.ads.AdRequest

class AdMobActivity : AppCompatActivity() {
    private var adMobHelper: AdMobHelper? = null
    private val buttonLoadInterstitialAd by lazy { findViewById<Button>(R.id.btn_load_interstitial) }
    private val buttonLoadRewardedVideoAd by lazy { findViewById<Button>(R.id.btn_load_rewarded_video) }
    private val buttonSettingConsentAd by lazy { findViewById<Button>(R.id.btn_setting_consent) }
    private val buttonRevokeConsentAd by lazy { findViewById<Button>(R.id.btn_revoke_consent) }
    private val adMobNativeView by lazy { findViewById<AdMobNativeView>(R.id.admob_native_view) }
    private val adMobBannerView by lazy { findViewById<AdMobBannerView>(R.id.admob_banner_view) }

    private fun initViews(){
        buttonLoadInterstitialAd.setOnClickListener {
            adMobHelper?.loadInterstitialAfterEndCount(this, it.id.toString(), 1, object : AdMobInterstitialLoadCallback() {
                    override fun onAdLoaded() {
                        super.onAdLoaded()
                        AdMobLogDebug.d("ADMOB", "InterstitialAd onAdLoaded()")
                    }

                    override fun onAdFailedToLoad(error: String?) {
                        super.onAdFailedToLoad(error)
                        AdMobLogDebug.d("ADMOB", "InterstitialAd onAdFailedToLoad($error)")
                    }

                    override fun onAdDismissedFullScreenContent() {
                        super.onAdDismissedFullScreenContent()
                        AdMobLogDebug.d("ADMOB", "InterstitialAd onAdDismissedFullScreenContent()")
                    }
                })
            return@setOnClickListener
        }
        buttonLoadRewardedVideoAd.setOnClickListener {
            adMobHelper?.loadRewardedVideoAfterEndCount(
                this,
                it,
                1,
                object : AdMobRewardedLoadCallback() {
                    override fun onRewardedAdFailedToLoad(error: String?) {
                        super.onRewardedAdFailedToLoad(error)
                        AdMobLogDebug.d("ADMOB", "RewardedVideoAd onRewardedAdFailedToLoad()")
                    }

                    override fun onAdDismissedFullScreenContent() {
                        super.onAdDismissedFullScreenContent()
                        AdMobLogDebug.d("ADMOB", "RewardedVideoAd onAdDismissedFullScreenContent()")
                    }

                    override fun onUserEarnedReward(rewardItem: RewardAdItem) {
                        super.onUserEarnedReward(rewardItem)
                        AdMobLogDebug.d(
                            "ADMOB",
                            "RewardedVideoAd onUserEarnedReward(${rewardItem.type})"
                        )
                    }
                }, MediationNetwork.request(ApplovinAdapter::class.java, AppLovinExtras.Builder()
                        .setMuteAudio(true)
                        .build()))
            return@setOnClickListener
        }
        buttonSettingConsentAd.setOnClickListener {
            UserConsentSettingsActivity.start(this, privacyPolicy, publisherId)
        }
        buttonRevokeConsentAd.setOnClickListener {
            UserConsentSdk.getInstance(this).revokeUserConsentDialog()
        }
    }

    private fun initAds(){
        adMobHelper = AdMobHelper.Builder()
            .setContext(this)
            .setDebugTestAds(BuildConfig.DEBUG)
            .setTestDeviceIds(listOf(AdRequest.DEVICE_ID_EMULATOR))
            .setAppUnitId(resources.getString(R.string.test_admob_app_unit_id))
            .setAdViewUnitId(resources.getString(R.string.test_admob_banner_unit_id))
            .setInterstitialUnitId(resources.getString(R.string.test_admob_interstitial_unit_id))
            .setRewardedVideoUnitId(resources.getString(R.string.test_admob_rewarded_video_unit_id))
            .setNativeAdvancedUnitId(resources.getString(R.string.test_admob_native_advance_unit_id))
            .setLoadAnimationBeforeLoadAd(true)
            .setLoadAnimationBeforeLoadAdAccentColor(R.color.colorPrimaryDark)
            .setAdViewAdaptiveSize(MediationNetwork.AdSize.adaptiveSize(this, windowManager, adMobBannerView))
            .setEnableAds(true)
            .setEnableBannerAd(true)
            .setEnableInterstitialAd(true)
            .setEnableRewardedVideoAd(true)
            .setNativeAdvancedStyles(AdMobNativeTemplateStyle.Builder()
                .withMainBackgroundColor(ColorDrawable(Color.parseColor("#C8ffffff")))
                .build())
            .setAdRequest(MediationNetwork.request(FacebookAdapter::class.java, FacebookExtras()
                .setNativeBanner(true)
                .build()))
            .build()

        /*adMobHelper?.loadNativeAd(
            nativeAdView, MediationNetwork.request(
                FacebookAdapter::class.java,
                extras
            )
        )*/
        adMobHelper?.loadAdView(adMobBannerView, object : AdMobViewLoadCallback() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                AdMobLogDebug.d("BANNER_VIEW", "onAdLoaded(${MediationNetwork.AdSize.adaptiveSize(applicationContext, windowManager, adMobBannerView)})")
            }
        }, MediationNetwork.request(FacebookAdapter::class.java, FacebookExtras()
            .setNativeBanner(true)
            .build()))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initAds()
        initViews()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.show_suite -> {} // MediationTestSuite.launch(this)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPause() {
        super.onPause()
        adMobHelper?.pause()
    }

    override fun onResume() {
        super.onResume()
        adMobHelper?.resume()
    }

    override fun onDestroy() {
        adMobHelper?.destroy()
        super.onDestroy()
    }
}
