package com.codeiseasydev.admobhelper

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.google.android.ads.admob.AdMobInitializeHelper

class MyApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        //AdMobInitializeHelper.initialize(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}